﻿#include "Game.hpp"
#include "GameModel.hpp"
#include "models/Game.pb.h"

#include <fstream>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/text_format.h>

#include <log4cplus/configurator.h>
#include <log4cplus/initializer.h>
#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

int main()
{
    // Initialization and deinitialization.
    log4cplus::Initializer initializer;

    log4cplus::BasicConfigurator configg;
    configg.configure();

    monopoly::model::Game gameModel;
    auto boardModel = gameModel.mutable_board();
    auto field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_START);
    field->add_parameters(200);

    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);
    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);
    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);
    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);
    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);

    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_PENALTY);
    field->add_parameters(50);

    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);
    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);
    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);
    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);
    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);

    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_REWARD);
    field->add_parameters(50);

    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);
    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);
    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);
    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);
    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);

    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_DEPOSIT);
    field->add_parameters(25);

    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);
    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);
    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);
    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);
    field = boardModel->add_fields();
    field->set_type(monopoly::model::Field_Type::Field_Type_EMPTY);

    auto bucketModel = gameModel.mutable_bucket();
    auto dice = bucketModel->add_dices();
    dice->set_type(monopoly::model::Dice_Type::Dice_Type_BINARY);
    dice->add_parameters(0);
    dice->add_parameters(1);

    dice = bucketModel->add_dices();
    dice->set_type(monopoly::model::Dice_Type::Dice_Type_BINARY);
    dice->add_parameters(1);
    dice->add_parameters(2);

    auto playersManager = gameModel.mutable_players_manager();
    auto player = playersManager->add_players();
    player->set_name("Player one");
    player->set_initial_cash(1000);

    player = playersManager->add_players();
    player->set_name("Player two");
    player->set_initial_cash(1000);

    {
        std::ofstream output("defaultModel.txt");
        std::string outStr;
        google::protobuf::TextFormat::PrintToString(gameModel, &outStr);
        output << outStr;
    }

    GameModel gameModelObj(gameModel);
    auto game(gameModelObj.CreateObject());
    game->SimulateGame(40);
    return 0;
}
