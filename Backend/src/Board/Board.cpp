#include "Board.hpp"

#include "BoardModel.hpp"

#include <iostream>

Board::Board(BoardModel& boardModel) : Loggable("BOARD")
{
    Loggable::CreationTracer creationTracker(*this, false);

    for (auto& fieldModel : boardModel.fieldModels())
    {
        _fields.emplace_back(fieldModel.CreateObject());
    }
}
