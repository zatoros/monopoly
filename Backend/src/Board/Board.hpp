#pragma once

#include "BoardIterator.hpp"
#include "BoardTypes.hpp"

#include "Loggable.hpp"

class BoardModel;

class Board : private Loggable
{
public:
    Board(BoardModel& boardModel);
    BoardIterator GetIterator() { return BoardIterator(_fields); }

protected:
    std::string ToString() const override { return ""; }
    std::string ObjName() const override { return "Board"; }

private:
    Fields _fields;
};
