#pragma once

#include "BoardTypes.hpp"

class BoardIterator
{
public:
    BoardIterator(Fields& container) : _begin(container.begin()), _end(container.end()), _curr(_begin) {}

    BoardIterator(const BoardIterator& other) = default;

    Field& operator*() { return **_curr; }

    BoardIterator& operator++()
    {
        if (++_curr == _end)
            _curr = _begin;

        return *this;
    }

private:
    using FieldsIterator = Fields::iterator;

    const FieldsIterator _begin;
    const FieldsIterator _end;
    FieldsIterator _curr;
};
