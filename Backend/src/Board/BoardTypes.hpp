#pragma once

#include "Field.hpp"
#include <memory>
#include <vector>

using FieldPtr = std::unique_ptr<Field>;
using Fields = std::vector<FieldPtr>;
