#include "BoardModel.hpp"

#include "Board.hpp"
#include "models/Board.pb.h"

BoardModel::BoardModel(const monopoly::model::Board& boardModel) : _fieldModels(boardModel.fields().begin(), boardModel.fields().end()) {}

BoardModel::ModelPtr BoardModel::CreateObject() { return createObject(*this); }
