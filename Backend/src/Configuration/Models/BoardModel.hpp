#pragma once

#include "FieldModel.hpp"
#include "Model.hpp"

#include <vector>

namespace monopoly::model
{
class Board;
}

class Board;

class BoardModel : public Model<Board>
{
public:
    using FieldModels = std::vector<FieldModel>;

    BoardModel(const monopoly::model::Board& boardModel);

    ModelPtr CreateObject() override;

    FieldModels& fieldModels() { return _fieldModels; }

private:
    FieldModels _fieldModels;
};
