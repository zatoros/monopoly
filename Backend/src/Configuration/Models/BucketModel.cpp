#include "BucketModel.hpp"

#include "Bucket.hpp"
#include "models/Bucket.pb.h"

BucketModel::BucketModel(const monopoly::model::Bucket& bucketModel) : _diceModels(bucketModel.dices().begin(), bucketModel.dices().end()) {}

BucketModel::ModelPtr BucketModel::CreateObject() { return createObject(*this); }
