#pragma once

#include "DiceModel.hpp"
#include "Model.hpp"

namespace monopoly::model
{
class Bucket;
}

class Bucket;

class BucketModel : public Model<Bucket>
{
public:
    using DiceModels = std::vector<DiceModel>;

    BucketModel(const monopoly::model::Bucket& bucketModel);

    ModelPtr CreateObject() override;

    DiceModels& diceModels() { return _diceModels; }

private:
    DiceModels _diceModels;
};
