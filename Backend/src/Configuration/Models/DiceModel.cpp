#include "DiceModel.hpp"

#include "BinaryDice.hpp"
#include "Dice.hpp"
#include "models/Bucket.pb.h"

DiceModel::DiceModel(const monopoly::model::Dice& diceModel)
    : _diceType(static_cast<DiceModel::DiceType>(diceModel.type())), _parameters(diceModel.parameters().begin(), diceModel.parameters().end())
{
}

DiceModel::ModelPtr DiceModel::CreateObject()
{
    switch (_diceType)
    {
    case DiceModel::DiceType::BINARY:
        return createObjectType<BinaryDice>(BinaryDiceResult{_parameters[0], _parameters[1]});
        break;
    }
    return {};
}
