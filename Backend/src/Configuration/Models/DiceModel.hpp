#pragma once

#include "Model.hpp"
#include "Types.hpp"

#include <vector>

namespace monopoly::model
{
class Dice;
}

class Dice;

class DiceModel : public Model<Dice>
{
public:
    DiceModel(const monopoly::model::Dice& diceModel);

    ModelPtr CreateObject() override;

private:
    enum class DiceType
    {
        BINARY = 0,
    } _diceType;

    std::vector<Dots> _parameters;
};
