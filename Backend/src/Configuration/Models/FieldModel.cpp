#include "FieldModel.hpp"

#include "Field.hpp"
#include "models/Board.pb.h"
#include <Deposit.hpp>
#include <Penalty.hpp>
#include <Reward.hpp>
#include <Start.hpp>

#include <functional>
#include <map>

FieldModel::FieldModel(const monopoly::model::Field& fieldModel)
    : _fieldType(static_cast<FieldModel::FieldType>(fieldModel.type())), _parameters(fieldModel.parameters().begin(), fieldModel.parameters().end())
{
}

FieldModel::ModelPtr FieldModel::CreateObject()
{
    switch (_fieldType)
    {
    case FieldModel::FieldType::EMPTY:
        return createObjectType<Field>();
        break;
    case FieldModel::FieldType::DEPOSIT:
        return createObjectType<Deposit>(_parameters[0]);
        break;
    case FieldModel::FieldType::PENALTY:
        return createObjectType<Penalty>(_parameters[0]);
        break;
    case FieldModel::FieldType::REWARD:
        return createObjectType<Reward>(_parameters[0]);
        break;
    case FieldModel::FieldType::START:
        return createObjectType<Start>(_parameters[0]);
        break;
    }
    return {};
}
