#pragma once

#include "Model.hpp"
#include "Types.hpp"

#include <vector>

namespace monopoly::model
{
class Field;
}

class Field;

class FieldModel : public Model<Field>
{
public:
    FieldModel(const monopoly::model::Field& fieldModel);

    ModelPtr CreateObject() override;

private:
    enum class FieldType
    {
        EMPTY = 0,
        DEPOSIT = 1,
        PENALTY = 2,
        REWARD = 3,
        START = 4,
    } _fieldType;

    std::vector<Cash> _parameters;
};
