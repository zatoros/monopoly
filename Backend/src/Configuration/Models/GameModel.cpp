#include "GameModel.hpp"

#include "Game.hpp"
#include "models/Game.pb.h"

GameModel::GameModel(const monopoly::model::Game& gameModel)
    : _boardModel(gameModel.board()), _playersManagerModel(gameModel.players_manager()), _bucketModel(gameModel.bucket())
{
}

GameModel::ModelPtr GameModel::CreateObject() { return createObject(*this); }
