#pragma once

#include "BoardModel.hpp"
#include "BucketModel.hpp"
#include "Model.hpp"
#include "PlayersManagerModel.hpp"

#include <BoardTypes.hpp>
#include <DicesTypes.hpp>

class Game;

namespace monopoly::model
{
class Game;
}

class GameModel : Model<Game>
{
public:
    GameModel(const monopoly::model::Game& gameModel);

    ModelPtr CreateObject() override;

    BoardModel& boardModel() { return _boardModel; }
    PlayersManagerModel& playersManagerModel() { return _playersManagerModel; }
    BucketModel& bucketModel() { return _bucketModel; }

private:
    BoardModel _boardModel;
    PlayersManagerModel _playersManagerModel;
    BucketModel _bucketModel;
};
