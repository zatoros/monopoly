#pragma once

#include <concepts>

#include <memory>

template <typename T, typename... ArgsT>
class Model
{
public:
    using ModelPtr = std::unique_ptr<T>;

    virtual ~Model() = default;

    virtual ModelPtr CreateObject(ArgsT... args) = 0;

    virtual void Validate() {}

protected:
    template <typename... mArgsT>
    ModelPtr createObject(mArgsT&&... args)
    {
        return std::make_unique<T>(std::forward<mArgsT>(args)...);
    }

    template <typename U, typename... mArgsT>
    ModelPtr createObjectType(mArgsT&&... args)
    {
        static_assert(std::derived_from<U, T> == true);
        return std::make_unique<U>(std::forward<mArgsT>(args)...);
    }
};
