#include "PlayerModel.hpp"

#include "Player.hpp"
#include "models/Player.pb.h"

#include <utility>

PlayerModel::PlayerModel(const monopoly::model::Player& playerModel) : _name(playerModel.name()), _initialCash(playerModel.initial_cash()) {}

PlayerModel::ModelPtr PlayerModel::CreateObject(BoardIterator&& boardIterator) { return createObject(*this, std::forward<BoardIterator>(boardIterator)); }
