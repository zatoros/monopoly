#pragma once

#include "Model.hpp"
#include "Types.hpp"

namespace monopoly::model
{
class Player;
}

class Player;
class BoardIterator;

class PlayerModel : public Model<Player, BoardIterator&&>
{
public:
    PlayerModel(const monopoly::model::Player& playerModel);

    ModelPtr CreateObject(BoardIterator&& boardIterator) override;

    const Name& name() const { return _name; }
    Cash initialCash() const { return _initialCash; }

private:
    Name _name;
    Cash _initialCash;
};
