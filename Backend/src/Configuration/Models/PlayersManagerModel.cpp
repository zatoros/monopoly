#include "PlayersManagerModel.hpp"

#include "PlayersManager.hpp"
#include "models/Player.pb.h"

PlayersManagerModel::PlayersManagerModel(const monopoly::model::PlayersManager& playersManagerModel)
    : _playerModels(playersManagerModel.players().begin(), playersManagerModel.players().end())
{
}

PlayersManagerModel::ModelPtr PlayersManagerModel::CreateObject(Board& board) { return createObject(*this, board); }
