#pragma once

#include "Model.hpp"
#include "PlayerModel.hpp"

#include <vector>

namespace monopoly::model
{
class PlayersManager;
}

class PlayersManager;
class Board;

class PlayersManagerModel : public Model<PlayersManager, Board&>
{
public:
    using PlayerModels = std::vector<PlayerModel>;

    PlayersManagerModel(const monopoly::model::PlayersManager& playersManagerModel);

    ModelPtr CreateObject(Board& board) override;

    PlayerModels& playerModels() { return _playerModels; }

private:
    PlayerModels _playerModels;
};
