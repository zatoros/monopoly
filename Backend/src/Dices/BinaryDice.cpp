#include "BinaryDice.hpp"

#include <random>

#include <iostream>

Dots BinaryDice::Throw() const
{
    logTrace("throwing");

    static constexpr int DISTRIBUTION_RANGE = 1000;
    static std::random_device randomDevice;
    static std::default_random_engine randomEngine(randomDevice());
    static std::uniform_int_distribution<int> uniform_dist(0, DISTRIBUTION_RANGE);

    int randomNumber = uniform_dist(randomEngine);
    Dots result = (randomNumber < DISTRIBUTION_RANGE / 2) ? _result.first : _result.second;

    logTrace(std::format("result: {}", result));
    return result;
}

std::string BinaryDice::ToString() const { return std::format("[ {}, {} ]", _result.first, _result.second); }
