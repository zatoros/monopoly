#pragma once

#include "Dice.hpp"

#include <utility>

using BinaryDiceResult = std::pair<Dots, Dots>;

#include <iostream>

class BinaryDice : public Dice
{
public:
    BinaryDice(const BinaryDiceResult& binaryDiceResult = BinaryDiceResult{1, 2}) : _result(binaryDiceResult) { Loggable::CreationTracer creationTracker(*this); }
    Dots Throw() const override;

protected:
    std::string ToString() const override;
    std::string ObjName() const override { return "BinaryDice"; }

private:
    BinaryDiceResult _result;
};
