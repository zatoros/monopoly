#include "Bucket.hpp"

#include <numeric>

#include "BucketModel.hpp"
#include <iostream>

Bucket::Bucket(BucketModel& bucketModel)
{
    Loggable::CreationTracer creationTracker(*this, false);
    for (auto& diceModel : bucketModel.diceModels())
    {
        _dices.emplace_back(diceModel.CreateObject());
    }
}

Dots Bucket::Throw() const
{
    logTrace("throwing");

    Dots result = std::accumulate(_dices.begin(), _dices.end(), Dots(), [](const Dots& result, const DicePtr& dice) { return result + dice->Throw(); });

    logTrace(std::format("result: {}", result));

    return result;
}
