#pragma once

#include "Dice.hpp"
#include "DicesTypes.hpp"
#include "Types.hpp"

class BucketModel;

class Bucket : public Dice
{
public:
    Bucket(BucketModel& bucketModel);
    Dots Throw() const override;

protected:
    std::string ToString() const override { return ""; }
    std::string ObjName() const override { return "Bucket"; }

private:
    Dices _dices;
};
