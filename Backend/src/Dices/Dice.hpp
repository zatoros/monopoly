#pragma once

#include "Loggable.hpp"
#include "Types.hpp"

class Dice : protected Loggable
{
public:
    Dice() : Loggable("DICE") {}
    virtual Dots Throw() const = 0;
};
