#pragma once

#include "Dice.hpp"

#include <memory>
#include <vector>

using DicePtr = std::unique_ptr<Dice>;
using Dices = std::vector<DicePtr>;
