#include <gtest/gtest.h>

#include "BinaryDice.hpp"

#include <memory>

const unsigned MAX_ROLL_COUNT = 1000;

TEST(DicesTest, BinaryDiceRoll)
{
    std::unique_ptr<BinaryDice> dice = std::make_unique<BinaryDice>();
    for (unsigned roll_count = 0; roll_count < MAX_ROLL_COUNT; roll_count++)
    {
        auto dots = dice->Throw();
        EXPECT_LE(dots, 2);
        EXPECT_GE(dots, 1);
    }
}
