#include "Deposit.hpp"
#include "PlayerFieldActions.hpp"

#include <iostream>

void Deposit::OnStep(PlayerFieldActions& player)
{
    Field::OnStep(player);
    player.GiveMoney(_deposit);
    _deposit = 0;
}

void Deposit::OnPass(PlayerFieldActions& player)
{
    Field::OnPass(player);
    _deposit += player.TakeMoney(_tax);
}

std::string Deposit::ToString() const { return std::format("[ tax:{}, deposit:{} ]", _tax, _deposit); }
