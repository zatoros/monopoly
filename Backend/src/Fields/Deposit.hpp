#pragma once

#include "Field.hpp"
#include "Types.hpp"

#include <iostream>

class Deposit : public Field
{
public:
    Deposit(const Cash tax) : _tax(tax) { Loggable::CreationTracer creationTracker(*this); }
    void OnPass(PlayerFieldActions& player) override;
    void OnStep(PlayerFieldActions& player) override;

protected:
    std::string ToString() const override;
    std::string ObjName() const override { return "Deposit"; }

private:
    const Cash _tax;
    Cash _deposit{0};
};
