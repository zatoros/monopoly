#pragma once

#include "Loggable.hpp"

class PlayerFieldActions;

#include <string>

class Field : protected Loggable
{
public:
    Field() : Loggable("FIELD") { Loggable::CreationTracer creationTracker(*this); }

    virtual ~Field() = default;
    virtual void OnPass(PlayerFieldActions& player) { logTrace(std::format("Passed {}", ToString())); }
    virtual void OnStep(PlayerFieldActions& player) { logTrace(std::format("Stepped {}", ToString())); }

protected:
    std::string ToString() const override { return ""; }
    std::string ObjName() const override { return "Field"; }
};
