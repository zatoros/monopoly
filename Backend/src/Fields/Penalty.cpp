#include "Penalty.hpp"
#include "PlayerFieldActions.hpp"

#include <iostream>

void Penalty::OnStep(PlayerFieldActions& player)
{
    Field::OnStep(player);
    player.TakeMoney(_value);
}

std::string Penalty::ToString() const { return std::format("[ value:{}  ]", _value); }
