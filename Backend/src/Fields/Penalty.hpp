#pragma once

#include "Field.hpp"
#include "Types.hpp"

#include <iostream>

class Penalty : public Field
{
public:
    Penalty(const Cash value) : _value(value) { Loggable::CreationTracer creationTracker(*this); }
    void OnStep(PlayerFieldActions& player) override;

protected:
    std::string ToString() const override;
    std::string ObjName() const override { return "Penalty"; }

private:
    const Cash _value;
};
