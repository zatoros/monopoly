#include "Reward.hpp"
#include "PlayerFieldActions.hpp"

#include <iostream>

void Reward::OnStep(PlayerFieldActions& player)
{
    Field::OnStep(player);
    player.GiveMoney(_value);
}

std::string Reward::ToString() const { return std::format("[ value:{}  ]", _value); }
