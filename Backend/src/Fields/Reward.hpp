#pragma once

#include "Field.hpp"
#include "Types.hpp"

#include <iostream>

class Reward : public Field
{
public:
    Reward(Cash value) : _value(value) { Loggable::CreationTracer creationTracker(*this); }
    void OnStep(PlayerFieldActions& player) override;

protected:
    std::string ToString() const override;
    std::string ObjName() const override { return "Reward"; }

private:
    const Cash _value;
};
