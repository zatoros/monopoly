#include "Start.hpp"
#include "PlayerFieldActions.hpp"

#include <iostream>

void Start::OnPass(PlayerFieldActions& player)
{
    Field::OnStep(player);
    player.GiveMoney(_value);
}

std::string Start::ToString() const { return std::format("[ value:{}  ]", _value); }
