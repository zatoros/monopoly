#pragma once

#include "Field.hpp"
#include "Types.hpp"

#include <iostream>

class Start : public Field
{
public:
    Start(const Cash value) : _value(value) { Loggable::CreationTracer creationTracker(*this); }
    void OnPass(PlayerFieldActions& player) override;

protected:
    std::string ToString() const override;
    std::string ObjName() const override { return "Start"; }

private:
    const Cash _value;
};
