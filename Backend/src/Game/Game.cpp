#include "Game.hpp"

#include "GameModel.hpp"

#include <iostream>

Game::Game(GameModel& gameModel) : Loggable("GAME"), _board(gameModel.boardModel()), _playersManager(gameModel.playersManagerModel(), _board), _bucket(gameModel.bucketModel())
{
    Loggable::CreationTracer creationTracker(*this);
}

void Game::SimulateGame(Steps steps)
{
    logTrace("Started");
    while (steps-- && !_playersManager.GameOver())
    {
        logTrace(std::format("Rounds to finish: {}", steps));
        _playersManager.TakeTurn(_bucket);
        std::cin.get();
    }
    logTrace("Ended");
}
