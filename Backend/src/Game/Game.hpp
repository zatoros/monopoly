#pragma once

#include "Board.hpp"
#include "Bucket.hpp"
#include "Loggable.hpp"
#include "PlayersManager.hpp"
#include "Types.hpp"

class GameModel;

class Game : private Loggable
{
public:
    Game(GameModel& gameModel);
    void SimulateGame(Steps steps);

private:
    std::string ToString() const override { return ""; }
    std::string ObjName() const override { return ""; }

    Board _board;
    PlayersManager _playersManager;
    Bucket _bucket;
};
