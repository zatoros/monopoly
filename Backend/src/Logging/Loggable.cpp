#include "Loggable.hpp"

#include <format>

Loggable::CreationTracer::CreationTracer(const Loggable& loggable, bool onlyCreation) : _loggable(loggable)
{
    if (!onlyCreation)
        _loggable.logTrace("Creating");
}

Loggable::CreationTracer::~CreationTracer() { _loggable.logTrace(std::format("Created {}", _loggable.ToString())); }

Loggable::Loggable(const std::string& loggerName) : _loggerHeader(std::format("[{}]", loggerName)), _logger(log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("main"))) {}
