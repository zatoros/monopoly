#pragma once

#include <string>

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

class Loggable
{
public:
    Loggable(const std::string& loggerName);

protected:
    class CreationTracer
    {
    public:
        CreationTracer(const Loggable& loggable, bool onlyCreation = true);
        ~CreationTracer();

    private:
        const Loggable& _loggable;
    };

    void logTrace(const std::string& trace) const { LOG4CPLUS_WARN(_logger, std::format("{} {} {}", _loggerHeader, ObjName(), trace).c_str()); }

    virtual std::string ToString() const = 0;
    virtual std::string ObjName() const = 0;

private:
    std::string _loggerHeader;
    log4cplus::Logger _logger;
};
