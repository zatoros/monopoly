#include "Player.hpp"

#include "Active.hpp"
#include "Bankrupted.hpp"
#include "Field.hpp"

#include "PlayerModel.hpp"

#include <iostream>

Player::Player(PlayerModel& playerModel, BoardIterator&& initialPosition)
    : Loggable("PLAYER"), _name(playerModel.name()), _money(playerModel.initialCash()), _position(std::move(initialPosition))
{
    Loggable::CreationTracer creationTracker(*this, false);
    transitionTo<Active>();
}

void Player::TakeTurn(const Dots dots)
{
    logTrace(std::format("{} Takes turn {} dots", _name, dots));
    _state->MakeMove(dots);
}

void Player::GiveMoney(unsigned amount)
{
    logTrace(std::format("{} Gives money {}", _name, amount));
    _money += amount;
}

unsigned Player::TakeMoney(unsigned amount)
{
    logTrace(std::format("{} Takes money {}", _name, amount));
    if (_money >= amount)
    {
        _money -= amount;
        return amount;
    }
    else
    {
        Cash moneyTaken = _money;
        _money = 0;
        transitionTo<Bankrupted>();
        return moneyTaken;
    }
}

void Player::Pass() { (*(++_position)).OnPass(*this); }

void Player::Step() { (*_position).OnStep(*this); }

std::string Player::ToString() const { return std::format("[ name:{}, money:{} ]", _name, _money); }
