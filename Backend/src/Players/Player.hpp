#pragma once

#include "BoardIterator.hpp"
#include "BoardTypes.hpp"
#include "PlayerFieldActions.hpp"
#include "PlayerState.hpp"
#include "PlayerStateActions.hpp"

#include "Loggable.hpp"

#include <memory>

class PlayerState;
class PlayerModel;

class Player : private PlayerFieldActions, public PlayerStateActions, private Loggable
{
    using PlayerStatePtr = std::unique_ptr<PlayerState>;

public:
    Player(PlayerModel& playerModel, BoardIterator&& initialPosition);
    void TakeTurn(const Dots dots);

private:
    void GiveMoney(const Cash amount) override;
    Cash TakeMoney(const Cash amount) override;

    void Pass() override;
    void Step() override;

    std::string ToString() const override;
    std::string ObjName() const override { return "Player"; }

    template <typename T>
    void transitionTo()
    {
        static_assert(std::derived_from<T, PlayerState> == true);
        _state = std::make_unique<T>(*this);
    }

    const Name _name;
    Cash _money;
    PlayerStatePtr _state;
    BoardIterator _position;
};
