#pragma once

#include "Types.hpp"

class PlayerFieldActions
{
public:
    virtual ~PlayerFieldActions() = default;
    virtual void GiveMoney(const Cash amount) = 0;
    virtual Cash TakeMoney(const Cash amount) = 0;
};
