#pragma once

class PlayerStateActions
{
public:
    virtual void Pass() = 0;
    virtual void Step() = 0;
    virtual ~PlayerStateActions() = default;
};
