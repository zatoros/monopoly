#pragma once

#include "Player.hpp"
#include <memory>
#include <vector>

using PlayerPtr = std::unique_ptr<Player>;
using Players = std::vector<PlayerPtr>;
