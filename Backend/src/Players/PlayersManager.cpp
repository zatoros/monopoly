#include "PlayersManager.hpp"

#include "Board.hpp"
#include "Bucket.hpp"

#include "PlayerModel.hpp"
#include "PlayersManagerModel.hpp"

#include <iostream>

PlayersManager::PlayersManager(PlayersManagerModel& playersManagerModel, Board& board) : Loggable("PLAYER")
{
    Loggable::CreationTracer creationTracker(*this, false);
    for (auto& playerModel : playersManagerModel.playerModels())
    {
        _players.emplace_back(playerModel.CreateObject(board.GetIterator()));
    }
}

void PlayersManager::TakeTurn(const Bucket& bucket)
{
    for (auto& player : _players)
    {
        player->TakeTurn(bucket.Throw());
    }
}

bool PlayersManager::GameOver() const { return false; }
