#pragma once

#include "Loggable.hpp"
#include "Player.hpp"
#include "PlayerTypes.hpp"

class PlayersManagerModel;
class Board;
class Bucket;

class PlayersManager : private Loggable
{
public:
    PlayersManager(PlayersManagerModel& playersManagerModel, Board& board);
    void TakeTurn(const Bucket& bucket);
    bool GameOver() const;

private:
    std::string ToString() const override { return ""; };
    std::string ObjName() const override { return "PlayersManager"; }

    Players _players;
};
