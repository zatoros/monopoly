#include "Active.hpp"

#include <iostream>

void Active::MakeMove(Dots dots)
{
    logTrace(std::format("Moving dots {}", dots));
    if (dots == 0)
        return;

    while (dots--)
    {
        _actions.Pass();
    }
    _actions.Step();
}
