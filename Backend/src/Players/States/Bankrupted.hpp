#pragma once

#include "PlayerState.hpp"
#include "PlayerStateActions.hpp"

#include <iostream>

class Bankrupted : public PlayerState
{
public:
    Bankrupted(PlayerStateActions& actions) : PlayerState(actions) { Loggable::CreationTracer creationTracker(*this); }
    void MakeMove(Dots dots) override {}

protected:
    std::string ObjName() const override { return "Bankrupted"; }
};
