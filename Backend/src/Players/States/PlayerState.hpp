#pragma once

#include "Loggable.hpp"
#include "Types.hpp"

class PlayerStateActions;

class PlayerState : protected Loggable
{
public:
    virtual ~PlayerState() = default;
    PlayerState(PlayerStateActions& actions) : Loggable("PLAYER"), _actions(actions) {}
    virtual void MakeMove(Dots dots) = 0;

protected:
    std::string ToString() const override { return ""; }

    PlayerStateActions& _actions;
};
