import os
from conan import ConanFile
from conans.client.loader import ConanFileTextLoader
from conan.tools.cmake import cmake_layout

ENCODING = "utf-8"

INCLUDE_PATHS = ["Backend"]

REQUIRES = ["gtest/1.15.0"]


class MonopolyRecipe(ConanFile):
    """
    Main project recipe
    """
    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeDeps", "CMakeToolchain"

    def requirements(self):
        """
        Requirements list
        """
        requirements_list = REQUIRES
        for path in INCLUDE_PATHS:
            config_path = os.path.join(os.path.dirname(__file__), path,
                                       "conanfile.txt")
            config_file = open(config_path, 'r', encoding=ENCODING).read()
            config_loader = ConanFileTextLoader(config_file)
            requirements_list += config_loader.requirements
        for requirement in requirements_list:
            self.requires(requirement)

    def layout(self):
        """
        Default layout
        """
        cmake_layout(self)
